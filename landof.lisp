(defpackage "LAND.OF.LISP"
  (:use "COMMON-LISP"))

(in-package :land.of.lisp)

; første eksempel
(+ 3 (* 2 4))

; definerer to globale variabler
(defparameter *small* 1)
(defparameter *big* 100)

; defparameter overskriver *foo* 
(defparameter *foo* 5)
(defparameter *foo* 6) ; oppdateres til 6

; defvar overskriver ikke *foo* nå
(defvar *foo* 7) ; oppdateres ikke til 7

; definisjon av funksjon
; (defun function_name (arguments) ... )
(defun guess-my-number ()
  (ash (+ *small* *big*) -1))

; aritmetisk skift, blir 22
(ash 11 1)

; aritmetisk skift, blir 5
(ash 11 -1)

; funksjon som returnerer resultat av operasjon
(defun return-five () (+ 2 3))

; definisjon av smaller og bigger for spill
(defun smaller () 
  (setf *big* (1- (guess-my-number)))
  (guess-my-number))

(defun bigger ()
  (setf *small* (1+ (guess-my-number)))
  (guess-my-number))
  
; funksjon for å starte nytt spill ("nullstille" *small* og *big*)
(defun start-over()
  (defparameter *small* 1)
  (defparameter *big* 100)
  (guess-my-number))

; definisjon av lokale variabler
; (let (variable declarations)
; ...body...)

; skal returnere 11
(let ((a 5)
      (b 6))
   (+ a b))

;definisjon av lokale funksjoner
; (flet ((function_name (arguments)
;		...function body...))
;	...body...)

; skal returnere 15
(flet ((f (n)
	  (+ n 10)))
  (f 5))
  
; kan definere flere lokale funksjoner om gangen, skal returnere 12
(flet ((f (n)
      (+ n 10))
     (g (n)
      (- n 3)))
   (g (f 5)))

; for å gjøre funksjoner tilgjengelig i andre funksjoner som de er definert i lag med, kan labels brukes.
; skal returnere 21
(labels ((a (n) (+ n 5))
	 (b (n) (+ (a n) 6)))
  (b 10))

; Kapittel 3

(defun square (n) (* n n))

; symboler er en fundamental datatype i lisp (skal returnere T, de er like)
(eq 'fooo 'foOo)

; Tall, flyttall defineres ved bruk av punktum. Sum av heltall og flyttall blir flyttal (returnerer 2.0)
(+ 1 1.0)

; beregner 53 opphøyd i 53. Lisp støtter så store tall som du vil
(expt 53 53)

; divisjon og brøk. Dersom du gjør divisjon, får du en brøkdatatype tilbake (returnerer 2/3):
(/ 4 6)

; men dersom en involverer flyttall, blir det kun "vanlig datamaskin" divisjon (returnerer 0.66667):
(/ 4 6.0)

; Siste grunnleggende datatype er tekststrenger (skriver ut tekststreng på en måte som er fornuftig for brukeren):
(princ "Tutti frutti")
(princ "He yelled \"Stop that thief!\" from the busy street.")

; kode modus og data modus, kjører expt som opphøyer to i tre, returnerer 8
(expt 2 3)
(expt 2 (+ 3 4)) ; returnerer 128

; data modus, returnerer (expt 2 3). Blir ikke tolket/kjørt
'(expt 2 3)

; både kode og data er lister i lisp. En skjult cons funksjon blir brukt for å slå sammen lister
(cons 'chicken 'cat)
(cons 'chicken 'nil) ; lister er terminert med nil, dermed blir dette en-elements liste
(cons 'chicken ())   ; samme som ovenfor () == 'nil
(cons 'pork '(beef chicken)) ; samme som (pork beef chicken) liste
(cons 'beef (cons 'chicken ())) ; blir (beef chicken)
(cons 'pork (cons 'beef (cons 'chicken ()))) ; veldig tungvindt måte å definere (pork beef chicken), men det er slik det er definert internt

; car og cdr funksjonene: car plukker første element i lista, cdr plukker resten av elementene
(car '(pork beef chicken)) ; returnerer pork
(cdr '(pork beef chicken)) ; returnerer (beef chicken)

; car og cdr kan kombineres for å plukke elementer inni listen
(car (cdr '(pork beef chicken))) ; plukker beef
(cadr '(pork beef chicken)) ; cadr forkortelse for det som ble gjort ovenfor

; lister kan defineres med list kommandoen
(list 'pork 'beef 'chicken) ; 

; lister kan nøstes
'(cat (duck bat) ant) ; her er (duck bat) en liste inni den ytterste listen som midterste element

; lister kan være elementer i lister, i den sammenheng så vil car returnere en liste


(cddr '((peas carrots tomatoes) (pork beef chicken) duck)) ; returnerer listen (DUCK)
(caddr '((peas carrots tomatoes) (pork beef chicken) duck)) ; returnerer symbolet DUCK, (første element i (DUCK))

(cddar '((peas carrots tomatoes) (pork beef chicken) duck)) ; returnerer (TOMATOES)
(caddar '((peas carrots tomatoes) (pork beef chicken) duck)) ; returnerer TOMATOES
(cadadr '((peas carrots tomatoes) (pork beef chicken) duck)) ; returnerer BEEF

; Kapittel 4

(if '() 'i-am-true 'i-am-false) ; returnerer i-am-false
(if '(1) 'i-am-true 'i-am-false) ; returnerer i-am-true

(defun my-length (list) (if list (1+ (my-length (cdr list))) 0)) ; returnerer lengden på listen

(my-length '(list with four symbols)) ; returnerer 4

; det finnes 4 varianter av (): '(), (), 'nil og nil. Alle sammen er like

; if evaluerer og utfører kun en ting av gangen
(if (= (+ 1 2) 3) 'yup 'nope) ; returnerer yup
(if (= (+ 1 2) 4) 'yup 'nope) ; returnerer nope

; if kan brukes for å sjekke om lister er tomme

(if '(1) 'ikke-tom 'tom) ; returnerer ikke-tom
(if '() 'ikke-tom 'tom) ; returnerer tom

(if (oddp 5) 'odd-number 'even-number) ; returnerer odd-number

; if utfører kun den ene av 2 handlinger:

;(if (oddp 5) 'odd-number (/ 1 0)) ; her returneres 'odd-number, null divisionen unngås

; det er kun mulig å gjøre en ting om gangen i if setninger. Dersom en ønsker å gjøre flere ting kan en bruke progn

(defvar *number-was-odd* nil)
(if (oddp 5) (progn (setf *number-was-odd* t) 'odd-number) 'even-number)

; alternativer til if: when og unless

; when, utfør en eller flere handlinger. Som en if med støtte for flere handlinger, men uten else:
(defvar *number-is-odd* nil)
(when (oddp 5) (setf *number-is-odd* t) 'odd-number) ; setter variabel og returnerer 'odd-number

(unless (oddp 4) (setf *number-is-odd* nil) 'even-number) ; setter variabel og returnerer 'even-number

; kommandoen som gjør alt: cond

(defvar *arch-enemy* nil)
(defun pudding-eater (person) 
    (cond ((eq person 'henry) (setf *arch-enemy* 'stupid-lisp-alien) '(curse you lisp alien - you ate my pudding))
	  ((eq person 'johnny) (setf *arch-enemy* 'useless-old-johnny) '(i hope you choked on my pudding johnny))
	  (t '(why you eat my pudding stranger ?))))

; her kan case også brukes

(defun pudding-eater-case (person) 
    (case person
	  ((henry) (setf *arch-enemy* 'stupid-lisp-alien) '(curse you lisp alien - you ate my pudding))
	  ((johnny) (setf *arch-enemy* 'useless-old-johnny) '(i hope you choked on my pudding johnny))
	  (otherwise '(why you eat my pudding stranger ?))))

; and og or utfører ikke mer enn de behøver for å avgjøre om de skal returnere sann (T) eller falsk (nil)

(and (oddp 5) (oddp 7) (oddp 9)) ; returnerer T
(or (oddp 4) (oddp 7) (oddp 8)) ; returnerer T

(defparameter *is-it-even* nil)
(or (oddp 4) (setf *is-it-even* t)) ; her blir *is-it-even* satt
*is-it-even*
(defparameter *is-it-even* nil)
(or (oddp 5) (setf *is-it-even* t)) ; her blir *is-it-even* ikke sat
*is-it-even*

(defparameter *file-modified* nil) ; unngå å kjøre ugyldig kode
(if *file-modified* (if (ask-user-about-saving) (save-file))) 
; kan forenkles til
(and *file-modified* (ask-user-about-saving) (save-file)) ; siden and ikke gjør mer enn det som kreves for å avgjøre

; en mellomting fra de to forrige kodelinjene kan være:

(defun ask-user-about-saving () 'nil) ; blir brukt, må derfineres
(if (and *file-modified* (ask-user-about-saving)) (save-file))

; bruk av funksjoner som returnerer mer enn bare sannhet
(if (member 1 '(3 4 1 5)) 'one-is-in-the-list 'one-is-no-in-the-list);

; som forventet, men dersom en bruker member alene:
(member 1 '(3 4 1 5)); så returneres (1 5), altså resten av listen er med

; bruk av member med nil
(member nil '(3 4 nil 5)); 

; bruk av find-if

(find-if #'oddp '(2 4 5 6)) ; returnerer 5

; find-if kan både plukke elementer, eller returnere noe som virker som sann i if
(if (find-if #'oddp '(2 4 5 6)) 'odd 'even)

; dersom vi prøver den med nil direkte kan vi få problemer
(find-if #'null '(2 4 nil 6))

; Conrads regel for sammenlikning: bruk eq for å sammenlikne symboler, bruk equal for alt annet
(defparameter *fruit* 'apple)
(cond ((eq *fruit* 'apple) 'its-an-apple)
      ((eq *fruit* 'orange) 'its-an-orange))

; sammenlikning
(equal 'apple 'apple) ; T
(equal (list 1 2 3) (list 1 2 3)) ; T
(equal '(1 2 3) (cons 1 (cons 2 ( cons 3 ())))); T
(equal 5 5) ; T
(equal 2.5 2.5) ; T
(equal "foo" "foo") ; T
(equal #\a #\a) ; T

; equalp er mer sostifikert, kan sammenlikne flyttall og heltall, symboler med forskjellige størrelse på bokstaver
(equalp "Bob Smith" "bob smith"); T
(equalp 0 0.0);

; = brukes for all, string-equal for tekststrenger, char-equal for karakterer

; Kapittel 5 - Bygging av en tekst basert spillemotor

; definering av noder, lokasjoner/områder vi kan "besøke"
(defparameter *nodes* '((living-room (you are in the living-room. a wizard is snoring loudly on the couch.))
			(garden (you are in a beautiful garden. there is a well in front of you.))
			(attic (you are in the attic. there is a giant welding torch in the corner.))))

; for å beskrive ett område kan vi bruke assoc
(assoc 'garden *nodes*)

; lage funksjon

(defun describe-location (location nodes) (cadr (assoc location nodes)))

; beskrivelse av stier/veier mellom lokasjonene, hvordan en gjør det og med hva
(defparameter *edges* '((living-room (garden west door)
				     (attic upstairs ladder))
			(garden (living-room east door))
			(attic (living-room downstairs ladder))))

(defun describe-path (edge) `(there is a ,(caddr edge) going ,(cadr edge) from here.))

; ` stiller til datamodus, komma(,) stiller tilbake til kommandomodus, dermed kan en putte inn kommandoer inni en dataliste

(describe-path '(garden west door)) ; test

; beskrive flere stier på en gang:

(defun describe-paths (location edges) 
       (apply #'append (mapcar #'describe-path (cdr (assoc location edges)))))
; her skjer mange ting. Først, finn relevate stier:

(cdr (assoc 'living-room *edges*)) ; finner alle stier ut fra living-room

; deretter brukes mapcar for å kalle describe-path på hver stier

(mapcar #'describe-path '((garden west door) (attic upstairs ladder))) ; 

(describe-path '(garden west door)) ; test

; mapcar er en ny funksjon som kan forklares litt:

(mapcar #'sqrt '(1 2 3 4 5))

; mapcar tar inn en funksjon som den kaller på hvert element i en liste. Resultatet av hvert funksjonskall lagres også i en liste som så returneres.

(mapcar #'car '((foo bar) (baz qux))); plukker første element i hver liste og slår sammen til liste

; #' benyttes for å fortelle at en en skal bruke en funksjon som argument. Forkortelse for å bruke function kommandoen:

;

; Common Lisp har forskjellige navnerom for funksjoner og variabler. Best vist med eksempel:

(let (( car "Skoda Fabia"))
     (mapcar #'car '((foo bar) (baz qux)))) ; her defineres car som variabel med verdi men så brukes også den innebygde kommandoen car

; så.. append som ble brukt tidligere, slå sammen lister:

(append '(mary had) '(a) '(little lamb)) ; returnerer en liste som forventet

; siden vi har en nøstet liste med lister, så ønsker vi å ta å bruke hver indre liste som argument til append, dette gjøres med apply

(apply #'append '((mary had) (a) (little lamb))) ; i effekt samme virkning som forrige linje

; siste del av spillet er å lage en liste over objekter i spillet

(defparameter *objects* '(whiskey bucket frog chain))

; så lager vi en variabel som inneholder hvilken lokasjon hvert object tilhører

(defparameter *object-locations* '((whiskey living-room)
				   (bucket living-room)
				   (chain garden)
				   (frog garden)))

; så lage en funksjon som forteller om et objekt er i en gitt lokasjon.
(defun objects-at (loc objs obj-locs)
       (labels ((at-loc-p (obj)
			  (eq (cadr (assoc obj obj-locs)) loc)))
	       (remove-if-not #'at-loc-p objs)))
; funksjonen definerer en lokal funksjon at-loc-p som returnerer T om objektet er på lokasjonen nil ellers. Denne brukes til å fjerne objekter fra listen som ikke er der.

(objects-at 'living-room *objects* *object-locations*) ; (whisky bucket)

; beskrive objekter

(defun describe-objects (loc objs obj-loc)
       (labels ((describe-obj (obj)
			      `(you see a ,obj on the floor.)))
	       (apply #'append (mapcar #'describe-obj (objects-at loc objs obj-loc)))))


(describe-objects 'living-room *objects* *object-locations*)

; nå kan vi slå sammen alle disse beskrivelsefunksjonene til en look funksjon. Først definerer vi en variabel for å holde styr på spillerens posisjon

(defparameter *location* 'living-room) ; starter i living-room

(defun look ()
       (append (describe-location *location* *nodes*)
	       (describe-paths *location* *edges*)
	       (describe-objects *location* *objects* *object-locations*)))

(look) ; returnerer som forventet

; så definere en funksjon for å gå rundt

(defun walk (direction)
       (let ((next (find direction
			 (cdr (assoc *location* *edges*))
			 :key #'cadr)))
	    (if next
		(progn (setf *location* (car next))
		       (look))
		'(you cannot go that way.))))

; :key forteller find hva vi vil bruke som nøkkel når vi leter etter Y 
(find 'y '((5 x) (3 y) (7 z)) :key #'cadr) 

; lage kommando for å plukke opp ting
(defun pickup (object)
       (cond ((member object
		      (objects-at *location* *objects* *object-locations*))
	      (push (list object 'body) *object-locations*)
	      `(you are now carrying the ,object))
	      (t '(you cannot get that.))))

; bruk av push
(defparameter *foo* '(1 2 3))
(push 7 *foo*)

; vise hva en har plukket opp
(defun inventory () (cons 'items- (objects-at 'body *objects* *object-locations*)))

; kapittel 6

; skriver ut foo
(print "foo")

(progn (print "this") (print "is") (print "a") (print "test"))
(progn (prin1 "this") (prin1 "is") (prin1 "a") (prin1 "test"))

(defun say-hello ()
       (print "Please type your name:")
       (let ((name (read)))
	    (print "Nice to meet you, ")
	    (print name)))

(defun add-five ()
       (print "please enter a number:")
       (let ((num (read)))
	    (print "When I add five I get")
	    (print (+ num 5))))

(print '3)
(print '3.4)
(print 'foo)
(print '"foo")
(print '#\a)

; en kan lage symboler hvor bokstavstørrelse har noe å si ved å omringe det med | karakteren. Kan inneholde mellomrom og punktum
(defparameter | Dette er ett gyldig lisp symbol.| 7)

; for å skrive ut og lese inn lisp symboler på en måte som maskinen kan tolke brukes print og read. For å skrive ut tekst til bruker, kan det lønne seg å bruke princ
(progn (princ "This sentence will be interrupted")
       (princ #\newline)
       (princ "by an annoying newline character"))

;data modus
'(+ 1 2)

;kode modus
(+ 1 2)

(defparameter *foo* '(+ 1 2)) ; definerer dette som data
(eval *foo*) ; utfører data som kode

;(defun game-repl () (loop (print (eval (read)))))

(defun game-repl ()
       (let ((cmd (game-read)))
	    (unless (eq (car cmd) 'quit)
		    (game-print (game-eval cmd))
		    (game-repl))))

(defun game-read ()
       (let ((cmd (read-from-string
			  (concatenate 'string "(" (read-line) ")"))))
	    (flet ((quote-it (x)
			     (list 'quote x)))
	    (cons (car cmd) (mapcar #'quote-it (cdr cmd))))))

(defparameter *allowed-commands* '(look walk pickup inventory help))

(defun help () '(Valid commands are look walk pickup inventory quit help))

(defun game-eval (sexp)
       (if (member (car sexp) *allowed-commands*)
	   (eval sexp)
	   '(i do not know that command.)))

(defun tweak-text (lst caps lit)
       (when lst
	     (let ((item (car lst))
		  (rest (cdr lst)))
	     (cond ((eql item #\space) (cons item (tweak-text rest caps lit)))
		   ((member item '(#\! #\? #\.)) (cons item (tweak-text rest t lit)))
		   ((eql item #\") (tweak-text rest caps (not lit)))
		   (lit (cons item (tweak-text rest nil lit)))
		   (caps (cons (char-upcase item) (tweak-text rest nil lit)))
		   (t (cons (char-downcase item) (tweak-text rest nil nil)))))))

(defun game-print (lst)
       (princ (coerce (tweak-text (coerce (string-trim "() "
						       (prin1-to-string lst))
					  'list)
				  t
				  nil)
		      'string))
       (fresh-line))

; kapittel 6.5 - lambda funksjoner


(defun half (n) (/ n 2)) ; definisjon av funksjon

#'half

(lambda (n) (/ n 2)) ; anonym lambda funksjon som gjør det samme som forrige linje

(mapcar (lambda (n) (/ n 2)) '(2 4 6)) ; bruk av anonym lambda funksjon

; kapittel 7

(cons 1 (cons 2 (cons 3 nil))); vanlig liste

(cons 1 (cons 2 3)) ; returnerer (1 2 . 3)

'(1 . (2 . (3 . nil))) ; mer tungvindt måte å definere vanlig liste på

; cons kan brukes til å representere par

(cons 1 2) ; (1 . 2), car og cdr henholdsvis plukke første og siste element

; sirkulære lister

(setf *print-circle* t) ; denne må skrus på for å kunne skrive ut sirkulære lister

(defparameter foo '(1 2 3))
(setf (cdddr foo) foo) ; sett siste element i denne listen til å referere til denne listen

; en kan lage alist (association list) ut av par

(defparameter *drink-order* '((bill . double-espresso)
			      (lisa . small-drip-coffe)
			      (john . medium-latte)))

(assoc 'lisa *drink-order*)
(push '(lisa . large-mocha-with-whipped-cream) *drink-order* )
(assoc 'lisa *drink-order*)

*drink-order*

(defparameter *house* '((walls (mortar (cement) (water) (sand))
			 (bricks))
			(windows (glass) (frame) (curtains))
			(roof (shingles)
			 (chimney))))
*house*

(defparameter *wizard-nodes* '((living-room (you are in the living-room. a wizard is snoring loudly on the couch.))
			(garden (you are in a beautiful garden. there is a well in front of you.))
			(attic (you are in the attic. there is a giant welding torch in the corner.))))

(defparameter *wizard-edges* '((living-room (garden west door)
				     (attic upstairs ladder))
			(garden (living-room east door))
			(attic (living-room downstairs ladder))))

(defun dot-name (exp) (substitute-if #\_ (complement #'alphanumericp) (prin1-to-string exp)))

(substitute-if #\e #'digit-char-p "I'm a l33t hack3r!")
(dot-name 'living-room)

(substitute-if 0 #'oddp '(1 2 3 4 5 6 7 8 9))

(defparameter *max-label-length* 30)

(defun dot-label (exp) 
  (if exp
      (let ((s (write-to-string exp :pretty nil)))
	(if (> (length s) *max-label-length*)
	(concatenate 'string (subseq s 0 (- *max-label-length* 3)) "...")
	s))
  ""))

(dot-label "test!#¤%&")

(defun nodes->dot (nodes)
  (mapc (lambda (node)
	  (fresh-line)
	  (princ (dot-name (car node)))
	  (princ "[label=\"")
	  (princ (dot-label node))
	  (princ "\"];"))
	nodes))

(nodes->dot *wizard-nodes*)

(defun edges->dot (edges)
  (mapc (lambda (node)
	  (mapc (lambda (edge)
		  (fresh-line)
		  (princ (dot-name (car node)))
		  (princ "->")
		  (princ (dot-name (car edge)))
		  (princ "[label=\"")
		  (princ (dot-label (cdr edge)))
		  (princ "\"];"))
		(cdr node)))
	  edges))

(edges->dot *wizard-edges*)

(defun graph->dot (nodes edges)
  (princ "digraph{")
  (nodes->dot nodes)
  (edges->dot edges)
  (princ "}"))

(graph->dot *wizard-nodes* *wizard-edges*)

(defun dot->png (fname thunk)
  (with-open-file (*standard-output*
		   fname
		   :direction :output
		   :if-exists :supersede)
    (funcall thunk))
  (ext:shell (concatenate 'string "dot -Tpng -O " fname)))

(with-open-file (my-stream
		 "testfile.txt"
		 :direction :output
		 :if-exists :supersede)
  (princ "Hello File!" my-stream))


















